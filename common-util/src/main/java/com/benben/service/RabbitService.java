package com.benben.service;

import com.alibaba.fastjson.JSONObject;
import com.benben.util.RabbitData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class RabbitService {
    private final static String PRIFIX = "ackmq:";
    private final static int TIME = 15; //分钟

        //@Autowired
    //private RedisTemplate<String, RabbitData> redisTemplate;//报错,不支持自定义类型
    @Autowired
    private RedisTemplate<String,String> redisTemplate;//不报错

    @Autowired
    private RabbitTemplate rabbitTemplate;


    /* @Description 发送消息(确认模式下)
     * @Param
     * @return
     */

    public Boolean sendMessage(String exchange, String routingKey, Object massage, int retryCount) {
        //创建对象
        RabbitData rabbitData = new RabbitData().
                setMassage(massage).
                setExchange(exchange).
                setRetryCount(retryCount).
                setRoutingKey(routingKey);
        String id = UUID.randomUUID().toString().replaceAll("-", "");
        rabbitData.setId(id);
        //存到redis
        redisTemplate.opsForValue().set(PRIFIX+id, JSONObject.toJSONString(rabbitData),TIME, TimeUnit.MINUTES);
        //发送消息
        rabbitTemplate.convertAndSend(exchange, routingKey, massage, rabbitData);
        return true;
    }

    /* @Description 发送延迟消息（确认模式下）
     * @Param
     * @return
     */
    public Boolean sendDelayMessage(String exchange, String routingKey, Object massage, int retryCount, int delayTime) {
        //创建对象
        RabbitData rabbitData = new RabbitData().
                setMassage(massage).
                setExchange(exchange).
                setRoutingKey(routingKey).
                setRetryCount(retryCount).
                setDelay(true).
                setDelayTime(delayTime);
        String id = UUID.randomUUID().toString().replaceAll("-", "");
        rabbitData.setId(id);
//        //存到redis
        redisTemplate.opsForValue().set(PRIFIX+id,JSONObject.toJSONString(rabbitData),TIME, TimeUnit.MINUTES);
//        //发送消息
        rabbitTemplate.convertAndSend(exchange, routingKey, massage, message -> {
            message.getMessageProperties().setDelay(rabbitData.getDelayTime()*1000);
            return message;
        }, rabbitData);
        return true;
    }
}
