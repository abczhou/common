package com.benben.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "返回结果")
@Accessors(chain = true)
public class Result<T> {
    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private T data;

    public static Result ok() {
        return Result.ok(null);
    }

    public static Result ok(String message) {
        return Result.ok(message, null);
    }
    public static <T> Result ok(T data) {
        return Result.ok("success", data);
    }
    public static <T> Result ok(String message, T data) {
        return Result.ok(200, message, data);
    }

    public static <T> Result ok(Integer code, String message, T data) {
        return build(code, message, data);
    }

    public static Result fail() {
        return Result.fail(null);
    }

   public static Result fail(String message) {
        return Result.fail(message, null);
    }

    public static <T> Result fail(String message, T data) {
        return Result.fail(201, message, data);
    }

    public static <T> Result fail(Integer code, String message, T data) {
        Result result = build(code, message, data);
        return result;
    }

    private static <T> Result build(Integer code, String message, T data) {
        Result result = new Result<>();
        result.setCode(code);
        result.setMessage(message);
        result.setData(data);
        return result;
    }
    public static Result notLogin() {
        Result result = build(401, "用户未登录！", 401);
        return result;
    }
}
