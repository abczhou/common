package com.benben.util;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.amqp.rabbit.connection.CorrelationData;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class RabbitData extends CorrelationData implements Serializable {
    private static final long serialVersionUID=1L;
    //  消息主体
    private Object massage;
    //  交换机
    private String exchange;
    //  路由键
    private String routingKey;
    //  重试次数
    private int retryCount = 5;
    //当前重试次数
    private int nowCount=0;
    //  消息类型  是否是延迟消息
    private boolean isDelay = false;
    //  延迟时间
    private int delayTime = 10;
}
