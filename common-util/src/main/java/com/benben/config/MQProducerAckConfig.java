package com.benben.config;

import com.alibaba.fastjson.JSONObject;
import com.benben.util.RabbitData;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Component
public class MQProducerAckConfig implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback {

    private final static String PRIFIX = "ackmq:";
    @Resource
    private RabbitTemplate rabbitTemplate;

    /*   @Autowired
    private RedisTemplate<String, RabbitData> redisTemplate;//报错,序列化才行*/
    @Autowired
    private RedisTemplate<String, String> redisTemplate;//不报错

    @PostConstruct
    public void init() {
        this.rabbitTemplate.setConfirmCallback(this);
        this.rabbitTemplate.setReturnCallback(this);
    }

    /* @Description 是否到达交换机
     * @Param b true 到达  false 没到
     * @return
     */

    @Override
    public void confirm(CorrelationData correlationData, boolean b, String s) {
        //如果没到达交换机，调用重发方法
        if (!b) {
            //重发
            System.out.println("没有到达交换机重发！");
            this.againMessage(correlationData);
        }
    }


    /* @Description 是否到达队列，到达不执行，没到达执行
     * @Param
     * @return
     */
    @Override
    public void returnedMessage(Message massage, int i, String s, String s1, String s2) {
        //获取correlationDataId
        String correlationDataId = (String) massage.getMessageProperties().getHeaders().get("spring_returned_message_correlation");
        String str = redisTemplate.opsForValue().get(PRIFIX + correlationDataId);
        RabbitData rabbitData = JSONObject.parseObject(str, RabbitData.class);
        if (rabbitData != null) {
            System.out.println("没有到达队列重发！");
            this.againMessage(rabbitData);
        }
    }


    /* @Description 消息重发机制
     * @Param
     * @return
     */

    @SneakyThrows
    private void againMessage(CorrelationData correlationData) {
        RabbitData rabbitData = (RabbitData) correlationData;
        //获取信息
        int retryCount = rabbitData.getRetryCount();
        int nowCount = rabbitData.getNowCount();
        Object massage = rabbitData.getMassage();
        String exchange = rabbitData.getExchange();
        String routingKey = rabbitData.getRoutingKey();
        if (nowCount >= retryCount) return; //次数用完
        //睡眠一会
        Thread.sleep(1000);
        //重试次数加一
        rabbitData.setNowCount(nowCount + 1);
        //更新redis
        redisTemplate.opsForValue().set(PRIFIX + rabbitData.getId(), JSONObject.toJSONString(rabbitData));
        //判断是否是延迟消息
        boolean flag = rabbitData.isDelay();
        if (flag) {
            //发送延迟消息
            int delayTime = rabbitData.getDelayTime();
            rabbitTemplate.convertAndSend(exchange, routingKey, massage, message -> {
                message.getMessageProperties().setDelay(delayTime * 1000);
                return message;
            }, rabbitData);
        } else {
            //发送消息
            rabbitTemplate.convertAndSend(exchange, routingKey, massage,rabbitData);
        }
    }




}
