package com.benben.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
public class FutureConfig {


    @Bean
    public ThreadPoolExecutor threadPoolExecutor(){
        return  new ThreadPoolExecutor(100,1000,60, TimeUnit.SECONDS,new ArrayBlockingQueue<>(20000));
    }
    /*ThreadPoolExecutor参数
    核心线程数
    最大线程
    空闲存活时间
    时间单位
    阻塞队列
    默认：线程工厂
    拒绝策略
    * */

}
